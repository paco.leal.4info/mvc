<?php

namespace Mini\Model;

use Mini\Core\Model;

class Grade extends Model
{

    public function all()
    {
        $sql = "SELECT DISTINCT g2.*
                FROM grades as g1
                LEFT JOIN grades as g2
                ON (g2.parent_id=g1.id OR g1.id=g2.id)
                ORDER BY g2.id ASC, g2.parent_id ASC";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    public function create($name, $parent_id = null, $enrollment = null)
    {
        $now = date('Y-m-d H:i:s');
        $sql = "INSERT INTO grades (name, parent_id, enrollment, created_at, updated_at) VALUES (:name, :parent_id, :enrollment, :created_at, :updated_at)";
        $query = $this->db->prepare($sql);
        $parameters = array(':name' => $name, ':parent_id' => $parent_id, ':enrollment' => $enrollment, ':created_at' => $now, 'updated_at' => $now);
        if ($query->execute($parameters)) {
            return true;
        } else {
            return false;
        }
    }

    public function update($id, $enrollment)
    {
        $now = date('Y-m-d H:i:s');
        $sql = "UPDATE grades SET enrollment='$enrollment', updated_at='$now' WHERE id=$id";
        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $sql = "DELETE FROM grades
                WHERE id=$id";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function newPass($id, $pass)
    {
        $sql = "UPDATE grades SET enrollment = '$pass'
                WHERE id=$id";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll($id=null)
    {
        $sql = "SELECT DISTINCT g2.*
                FROM grades as g1
                INNER JOIN grades as g2
                ON (g2.parent_id=g1.id OR g1.id=g2.id)";
                if ($id != null) {
                    $sql .= " WHERE g2.id=$id OR g2.parent_id=$id";
                } else {
                    $sql .= " WHERE g2.parent_id IS NULL";
                }
                $sql .= " ORDER BY g2.id ASC, g2.parent_id ASC";
        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            // si existe $query->fetchAll(1) devolvemos fetchAll, si no devolvemos fetch ???
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    public function get($id)
    {
        $sql = "SELECT DISTINCT g2.*
                FROM grades as g1
                INNER JOIN grades as g2
                ON (g2.parent_id=g1.id OR g1.id=g2.id)
                WHERE g2.id=$id
                ORDER BY g2.id ASC, g2.parent_id ASC";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    public function getPosts($grade)
    {
        $name = $grade->name;
        $sql = "SELECT p.*, c.name as category, date_format(p.updated_at, '%b %d') as fecha
                FROM posts as p
                INNER JOIN categories as c
                ON category_id = c.id 
                INNER JOIN post_tag as pt
                ON pt.post_id=p.id
                INNER JOIN tags as t
                ON pt.tag_id=t.id WHERE status='published'
                AND t.name LIKE '$name'
                GROUP BY p.id";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    public function enroll($userid, $gradeid)
    {
        $sql = "INSERT INTO grade_user (user_id, grade_id)
                VALUES ($userid, $gradeid)";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function unroll($gradeid, $userid)
    {
        $sql = "DELETE FROM grade_user
                WHERE user_id=$userid AND grade_id=$gradeid";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
