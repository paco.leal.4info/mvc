<?php

namespace Mini\Model;
use Mini\Core\Model;
use Mini\Core\Session;

class Login extends Model
{
    public function doLogin($data)
    {
        if (!isset($data)) {
            Session::set('message', array('type' => 'danger', 'title' => 'Error', 'content' => 'No se han recibido los datos'));
            return false;
        }

        $sql = "SELECT users.id as id, users.name as name, email, password, GROUP_CONCAT(roles.name) as role, GROUP_CONCAT(grade_user.grade_id) as grades
                FROM users
                INNER JOIN role_user ON users.id = user_id
                INNER JOIN roles ON role_id = roles.id
                LEFT JOIN grade_user ON users.id = grade_user.user_id
                WHERE email=:email
                GROUP BY users.id";
        $query = $this->db->prepare($sql);
        $query->bindvalue(':email', $data['email']);
        $query->execute();

        $row = $query->rowCount();
        if ($row != 1) {
            Session::set('message', array('type' => 'danger', 'title' => 'Error', 'content' => 'El email o la contraseña no son correctos'));
            return false;
        }

        $usuario = $query->fetch();
        if ($usuario->password != md5($data['password'])) {
            Session::set('message', array('type' => 'danger', 'title' => 'Error', 'content' => 'El email o la contraseña no son correctos'));
            return false;
        }

        $usuario = (array) $usuario;

        if (strpos($usuario['role'], ',')) {
            $role = explode(',', $usuario['role']);
            $usuario['role'] = $role[0];
        }

        if (!empty($usuario['grades'])) {
            $usuario['grades'] = explode(',', $usuario['grades']);
        } else {
            $usuario['grades'][] = null;
        }

        unset($usuario['password']);
        Session::set('user', $usuario);
        Session::set('message', ['type' => 'info', 'title' => 'You just logged in!', 'content' => 'Welcome to AcademiaMVC']);
        return true;
    }
/*
    public function doRegister($data)
    {
        if (!isset($data)) {
            Session::set('mensaje', array('type' => 'danger', 'content' => 'No se han recibido los datos'));
            return false;
        }

        $sql = "INSERT INTO users (name, email, password, created_at, updated_at) VALUES (:name, :email, :password, :created_at, :updated_at)";
        $query = $this->db->prepare($sql);
        $query->bindvalue(':email', $data['email']);
        $query->execute();

        $row = $query->rowCount();
        if ($row != 1) {
            Session::set('mensaje', array('type' => 'danger', 'content' => 'El email o la contraseña no son correctos'));
            return false;
        }

        $usuario = $query->fetch();
        if ($usuario->password != md5($data['password'])) {
            Session::set('mensaje', array('type' => 'danger', 'content' => 'El email o la contraseña no son correctos'));
            return false;
        }

        $usuario = (array) $usuario;
        Session::set('user', $usuario);
        return true;
    }*/
}