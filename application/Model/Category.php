<?php

namespace Mini\Model;

use Mini\Core\Model;
use Mini\Core\Functions;

class Category extends Model
{
    public function all()
    {
        $sql = "SELECT * FROM categories";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    public function create($name)
    {
        $now = date('Y-m-d H:i:s');
        $slug = Functions::slug(strtolower($name));
        $sql = "INSERT INTO categories (name, slug, created_at, updated_at) VALUES (:name, :slug, :created_at, :updated_at)";
        $query = $this->db->prepare($sql);
        $parameters = array(':name' => $name, ':slug' => $slug, ':created_at' => $now, 'updated_at' => $now);
        if ($query->execute($parameters)) {
            return true;
        } else {
            return false;
        }
    }

    public function update($id, $name)
    {
        $slug = Functions::slug(strtolower($name));
        $sql = "UPDATE categories SET name='$name', slug='$slug', updated_at='" . date('Y-m-d H:i:s') . "' WHERE id=$id";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $sql = "DELETE FROM categories WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);
        if ($query->execute($parameters)) {
            return true;
        } else {
            return false;
        }

    }
}