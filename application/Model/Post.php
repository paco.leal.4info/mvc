<?php

namespace Mini\Model;

use Mini\Core\Model;
use Mini\Core\Session;
use Mini\Libs\Helper;

class Post extends Model
{
    public function getPosts($currentPage, $pagination = 5)
    {
        $firstVisiblePost = (($currentPage - 1) * $pagination);

        $sql = "SELECT p.*, c.name as category, u.name as author, date_format(p.updated_at, '%b %d') as fecha, GROUP_CONCAT(t.slug) as etiquetas from posts as p 
                INNER JOIN users as u ON user_id = u.id 
                INNER JOIN categories as c ON category_id = c.id 
                INNER JOIN post_tag as pt ON pt.post_id = p.id
                INNER JOIN tags as t ON pt.tag_id = t.id
                WHERE status = 'published'
                GROUP BY p.id
                ORDER BY updated_at DESC LIMIT $firstVisiblePost,$pagination";

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function search($what, $where)
    {
        $sql = "SELECT p.*, c.name as category, u.name as author, date_format(p.updated_at, '%b %d') as fecha, GROUP_CONCAT(t.slug) as etiquetas
                FROM posts as p 
                INNER JOIN users as u ON user_id = u.id 
                INNER JOIN categories as c ON category_id = c.id 
                INNER JOIN post_tag as pt ON pt.post_id = p.id
                INNER JOIN tags as t ON pt.tag_id = t.id
                WHERE status = 'published'
                GROUP BY p.id ";

        if ($what == 'category') {
            $sql .= " HAVING category LIKE '$where' ";
        } elseif ($what == 'tag') {
            $sql .= " HAVING etiquetas LIKE '%$where%' ";
        } else {
            return false;
        }

        $sql .= "ORDER BY updated_at DESC";

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function all()
    {
        $sql = "SELECT p.*, c.name as category, u.name as author, date_format(p.updated_at, '%b %d') as last_update, date_format(p.created_at, '%b %d') as created, GROUP_CONCAT(t.slug) as etiquetas from posts as p 
                INNER JOIN users as u ON user_id = u.id 
                INNER JOIN categories as c ON category_id = c.id 
                INNER JOIN post_tag as pt ON pt.post_id = p.id
                INNER JOIN tags as t ON pt.tag_id = t.id
                GROUP BY p.id";

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function getPost($slug)
    {
        $sql = "SELECT p.*, c.name as category, date_format(p.updated_at, '%b %d') as fecha, GROUP_CONCAT(t.slug) as etiquetas, GROUP_CONCAT(t.id) as tags FROM posts as p
                INNER JOIN categories as c ON category_id = c.id
                INNER JOIN post_tag as pt ON pt.post_id = p.id
                INNER JOIN tags as t ON pt.tag_id = t.id
                WHERE p.slug = '$slug' GROUP BY p.id";

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetch();
    }

    public function getNumberPosts($where = null)
    {
        if ($where) {
            $sql = "SELECT * FROM posts WHERE status = '$where'";
        } else {
            $sql = "SELECT * FROM posts";
        }
        //$sql = "SELECT COUNT(p.id) as row FROM posts AS p WHERE status = 'published'";

        $query = $this->db->prepare($sql);
        $query->execute();
        $total = $query->rowCount();

        return $total;
    }

    public function publish($id, $status)
    {
        $sql = "UPDATE posts SET status='$status', updated_at='" . date('Y-m-d H:i:s') . "' WHERE id=$id";
        $query = $this->db->prepare($sql);

        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $sql = "DELETE FROM posts WHERE id=$id";
        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAuthor($id)
    {
        $sql = "SELECT user_id AS author FROM posts WHERE id=$id";
        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    public function update($where, $params)
    {
        if (!empty($params)) {

            $fields = '';
            foreach ($params as $key => $value) {
                $fields .= $key . ' = :' . $key . ', ';
            }
            $fields = rtrim($fields, ', ');

            $key = key($where);
            $value = $where[$key];
            if (gettype($value) == 'string') {
                $value = "'$value'";
            }

            $ssql = 'UPDATE posts' .
                ' SET ' . $fields .
                ' WHERE ' . $key . '=' . $value;

            $prepare = $this->db->prepare($ssql);
            if ($prepare->execute($this->normalizePrepareArray($params))) {
                return true;
            } else {
                return false;
            }
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Parameters can not be empty']);
            return false;
        }
    }

    public function create($params)
    {
        if ( ! empty($params)) {

            $fields = '(' . implode(',', array_keys($params)) . ')';

            $values = "(:" . implode(",:", array_keys($params)) . ")";

            $ssql =  'INSERT INTO posts ' . $fields .
                ' VALUES ' . $values;

            $prepare = $this->db->prepare($ssql);

            $prepare->execute($this->normalizePrepareArray($params));

            //$this->setQuery($prepare);

            return $this->db->lastInsertId();

        } else {
            throw new Exception('Los parámetros están vacíos');
        }
    }

    public function linkTags($id, $tags)
    {
        $sql = "DELETE FROM post_tag WHERE post_id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();
        foreach ($tags as $tag) {
            $sql = "INSERT INTO post_tag (post_id, tag_id) VALUES ($id, $tag)";
            $query = $this->db->prepare($sql);
            $query->execute();
        }
        return true;
    }

    private function normalizePrepareArray($params)
    {
        foreach ($params as $key => $value) {
            $params[':' . $key] = $value;
            unset($params[$key]);
        }

        return $params;
    }
}