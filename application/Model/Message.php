<?php

namespace Mini\Model;

use Mini\Core\Model;

class Message extends Model
{

    public function all($id)
    {
        $sql = "SELECT m.*, u.name as author, mu.status as status, date_format(m.created_at, '%b %d, %H:%i:%s') as date
                FROM message_user as mu
                INNER JOIN messages as m ON mu.message_id=m.id
                INNER JOIN users as u ON m.author_id=u.id WHERE mu.user_id=$id";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }
    public function create($params)
    {
        $now = date('Y-m-d H:i:s');
        $sql = "INSERT INTO messages (author_id, subject, content, created_at, updated_at)
                VALUES (:author_id, :subject, :content, :created_at, :updated_at)";
        $query = $this->db->prepare($sql);
        $parameters = array(':author_id' => $_SESSION['user']['id'], ':subject' => $params['subject'], ':content' => $params['content'], ':created_at' => $now, 'updated_at' => $now);
        if ($query->execute($parameters)) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    public function linkUsers($id, $users)
    {
        foreach ($users as $user) {
            $sql = "INSERT INTO message_user (message_id, user_id) VALUES ($id, $user)";
            $query = $this->db->prepare($sql);
            $query->execute();
        }
        return true;
    }

    public function unread() {
        if (isset($_SESSION['user']['id'])) {
            $sql = "SELECT m.* FROM message_user as mu
                INNER JOIN messages as m ON mu.message_id=m.id
                WHERE mu.user_id=" . $_SESSION['user']['id'] . " AND mu.status='unread'";

            $query = $this->db->prepare($sql);
            if ($query->execute()) {
                return $query->rowCount();
            } else {
                return false;
            }
        }
    }

    public function read($id) {
        if (isset($_SESSION['user']['id'])) {
            $user_id = $_SESSION['user']['id'];
            $sql = "UPDATE message_user SET status = 'read' WHERE message_id=$id AND user_id=$user_id";

            $query = $this->db->prepare($sql);
            if ($query->execute()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function get($id)
    {
        $sql = "SELECT m.*, u.name as author, date_format(m.created_at, '%b %d, %H:%i:%s') as date
                FROM message_user as mu
                INNER JOIN messages as m ON mu.message_id=m.id
                INNER JOIN users as u ON m.author_id=u.id WHERE mu.message_id=$id";

        $query = $this->db->prepare($sql);
        if ($query->execute()) {
            return $query->fetch();
        } else {
            return false;
        }
    }

}