<?php

namespace Mini\Model;

use Mini\Core\Model;
//use Mini\Model\Login;

class User extends Model
{
    public function all()
    {
        $sql = "SELECT u.*, r.name AS role FROM users AS u 
                INNER JOIN role_user AS ru ON ru.user_id=u.id
                INNER JOIN roles AS r ON r.id=ru.role_id";

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function allSafe()
    {
        $sql = "SELECT u.*, r.name AS role FROM users AS u 
                INNER JOIN role_user AS ru ON ru.user_id=u.id
                INNER JOIN roles AS r ON r.id=ru.role_id
                WHERE r.name != 'admin'";

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function update($id, $params)
    {
        $sql = "UPDATE users SET name='$params[name]', email='$params[email]', updated_at='" . date('Y-m-d H:i:s') . "' WHERE id=$id";

        $query = $this->db->prepare($sql);
        $query->execute();

        $this->updateRole($id, $params['role_id']);
        return true;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM users WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);
    }

    public function create($params)
    {
        $users = New User();
        //validaciones {
        if ($users->getParam('email', $params['email'])) {
            // mensaje: el correo ya existe
            return false;
        }
        if ($params['password1'] != $params['password2'])
        {
            // mensaje: las contraseñas no coinciden
            return false;
        }
        //}
        $password = md5($params['password1']);
        $now = date('Y-m-d H:i:s');
        $sql = "INSERT INTO users (name, email, password, created_at, updated_at) VALUES ('$params[name]', '$params[email]', '$password', '$now', '$now')";
        $query = $this->db->prepare($sql);
        $query->execute();
        $id = $this->db->lastInsertId();
        $guestRole = 4;
        if ($id) {
            $sql = "INSERT INTO role_user (user_id, role_id, created_at, updated_at) VALUES ($id, '$guestRole', '$now', '$now')";
            $query = $this->db->prepare($sql);
            if ($query->execute()) {
                $login = New Login();
                $login->doLogin(['email' => $params['email'], 'password' => $params['password1']]);
                return true;
            } else {
                $this->delete($id);
                //mensaje: algo ha fallado, por favor inténtalo de nuevo más tarde
                return false;
            }
        }
    }

    public function getParam($where, $what)
    {
        $sql = "SELECT $where FROM users WHERE $where='$what'";

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetch();
    }

    public function roles()
    {
        $sql = "SELECT * FROM roles WHERE name!='admin'";

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function updateRole($user_id, $role_id)
    {
        $sql = "UPDATE role_user SET role_id='$role_id' WHERE user_id=$user_id";

        $query = $this->db->prepare($sql);
        $query->execute();

        return true;
    }

    public function course($courses)
    {
        $sql = "SELECT u.*, r.name AS role FROM users AS u 
                INNER JOIN role_user AS ru ON ru.user_id=u.id
                INNER JOIN roles AS r ON r.id=ru.role_id
                INNER JOIN grade_user as gu ON gu.user_id=u.id
                WHERE r.name != 'admin' && r.name != 'teacher' && ";
                foreach ($courses as $course) {
                    $sql .= "gu.grade_id=$course || ";
                }
                $sql = rtrim($sql, ' || ');

        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
}