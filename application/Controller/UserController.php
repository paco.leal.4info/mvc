<?php

namespace Mini\Controller;

use Mini\Core\Auth;
use Mini\Model\User;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkAuth('teacher');
    }

    public function index()
    {
        Auth::checkAuth('admin');
        $users = New User();
        $users = $users->allSafe();
        $roles = $this->getRoles();
        $this->view->addData(array('title' => 'User List', 'users' => $users, 'roles' => $roles));
        echo $this->view->render('user/index');
        return true;
    }

    public function update($id)
    {
        if ($_POST['role_id'] < 2 || $_POST['role_id'] > 4) {
            header('Location: /error');
        }
        // fin de las validaciones
        $user = New User();
        $user = $user->update($id, $_POST);
        if ($_SESSION['user']['role'] == 'teacher') {
            header('Location: /teacher/courses');
            return true;
        } else {
            $this->index();
        }
        return true;
    }

    public function delete($id)
    {
        $users = New User();
        $users = $users->delete($id);
        if ($_SESSION['user']['role'] == 'teacher') {
            header('Location: /teacher/courses');
            return true;
        } else {
            $this->index();
        }
        return true;
    }

    public function getRoles()
    {
        $roles = New User();
        $roles = $roles->roles();
        return $roles;
    }
}