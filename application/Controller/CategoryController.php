<?php

namespace Mini\Controller;

use Mini\Core\Auth;
use Mini\Core\Session;
use Mini\Model\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkAuth('admin');
    }

    public function index()
    {
        $categories = New Category();
        $categories = $categories->all();
        $this->view->addData(array('title' => 'Category List', 'categories' => $categories));
        echo $this->view->render('categories/index');
        return true;
    }

    public function create()
    {
        $categories = New Category();
        if ($categories = $categories->create($_POST['name'])) {
            Session::set('message', ['type' => 'success', 'title' => 'Success', 'content' => 'The new category was successfully created']);
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Failed trying to create category']);
        }
        $this->index();
        return true;
    }

    public function update($id)
    {
        $categories = New Category();
        if ($categories = $categories->update($id, $_POST['name'])) {
            Session::set('message', ['type' => 'success', 'title' => 'Success', 'content' => 'The category was successfully updated']);
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Failed trying to update category']);
        }
        $this->index();
        return true;
    }

    public function delete($id)
    {
        $categories = New Category();
        if ($categories = $categories->delete($id)) {
            Session::set('message', ['type' => 'success', 'title' => 'Success', 'content' => 'The category was successfully deleted']);
            $this->index();
            return true;
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Failed trying to delete category']);
            $this->index();
            return false;
        }
    }

    public function new()
    {
        $this->view->addData(array('title' => "Category Creation Form", 'route' => 'Category'));
        echo $this->view->render("categories/new");
        return true;
    }
}