<?php

namespace Mini\Controller;

use Mini\Core\Functions;
use Mini\Core\Session;
use Mini\Model\Category;
use Mini\Model\Post;
use Mini\Core\Auth;
use Mini\Model\Tag;

class PostController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkAuth('teacher');
    }

    public function index()
    {
        $posts = New Post();
        $posts = $posts->all();
        $this->view->addData(array('title' => 'Post List', 'posts' => $posts));
        echo $this->view->render('posts/index');
        return true;
    }

    public function publish($id)
    {
        $post = New Post();
        $post = $post->publish($id, $_POST['status']);
        if ($_POST['status'] == 'published') {
            Session::set('message', ['type' => "success", 'title' => 'Success', 'content' => "The post was just published"]);
        } else {
            Session::set('message', ['type' => "success", 'title' => 'Success', 'content' => "The post was withdrawn"]);
        }

        $this->index();
        return true;
    }

    public function delete($id)
    {

        $post = New Post();
        $author = $post->getAuthor($id);
        if ($_SESSION['user']['id'] == $author || Auth::checkAuth('admin', false)) {
            $post = $post->delete($id);
            Session::set('message', ['type' => "success", 'title' => 'Success', 'content' => "The post was correctly deleted"]);
            $this->index();
            return true;
        } else {
            header('Location: /error');
            return false;
        }
    }

    public function edit($slug)
    {
        $post = New Post();
        $post = $post->getPost($slug);
        $tags = New Tag();
        $tags = $tags->all();
        $categories = New Category();
        $categories = $categories->all();
        if ($_SESSION['user']['id'] == $post->user_id || $_SESSION['user']['role'] == 'admin') {
            $this->view->addData(array('title' => 'Edit Post', 'post' => $post, 'tags' => $tags, 'categories' => $categories));
            echo $this->view->render('posts/edit');
            return true;
        } else {
            header('Location: /error');
            return false;
        }
    }

    public function new($gradeName = null)
    {
        $tags = New Tag();
        $tags = $tags->all();
        $categories = New Category();
        $categories = $categories->all();
        $this->view->addData(array('title' => 'New Post','tags' => $tags, 'categories' => $categories, 'gradeName' => $gradeName));
        echo $this->view->render('posts/edit');
        return true;
    }

    public function update($id = null)
    {
        $post = New Post();
        $tags = $_POST['tags'];
        unset($_POST['tags']);
        $_POST['user_id'] = $_SESSION['user']['id'];
        $_POST['slug'] = Functions::slug($_POST['name']);
        $_POST['updated_at'] = date('Y-m-d H:i:s');

        if (isset($_POST['status']) && $_POST['status'] == true) {
            $_POST['status'] = 'published';
        }

        if ($id) {
            if ($post->update(['id' => $id], $_POST)) {
                $post->linkTags($id, $tags);
            } else {
                Session::set('message', ['type' => "danger", 'title' => 'Error', 'content' => "The database connection failed, try again later"]);
                return false;
            }
        } else {
            $_POST['created_at'] = $_POST['updated_at'];
            if ($creation_id = $post->create($_POST)) {
                $post->linkTags($creation_id, $tags);
            } else {
                Session::set('message', ['type' => "danger", 'title' => 'Error', 'content' => "The database connection failed, try again later"]);
                return false;
            }
        }

        Session::set('message', ['type' => "success", 'title' => 'Success', 'content' => "The post was successfully saved"]);

        if (isset($_FILES['file']) && !empty($_FILES['file'])) {
            if  ($id) {
                $this->file($_POST['slug'], $_FILES, true);
            } else {
                $this->file($_POST['slug'], $_FILES);
            }
        }

        $this->index();
        return true;
    }

    public function file($slug, $files, $delete = false)
    {
        //validamos
        if (true) {                                                     // si no hay errores al validar
            //var_dump($errores);
            $post = new Post();
            if ($delete) {
                $oldPath = $post->getPost($slug);
                $oldPath = $oldPath->file;
                if (strpos($oldPath, 'http') == 0) {
                    $delete = false;
                } else {
                    $oldPath = substr($oldPath, 1);
                }

            }
            $destino = 'img/' . date('dmY-H_i') . '-' . $files['file']['name'];
            //echo $destino;
            if (move_uploaded_file($files['file']['tmp_name'], $destino)) {
                $destino = '/' . $destino;
                if ($post->update(['slug' => $slug], ['file' => $destino])) {
                    if ($delete) {
                        unlink($oldPath);
                    }
                    Session::set('message', ['type' => "success", 'title' => 'Success', 'content' => "The post and the file were saved without any issues"]);
                    return true;
                } else {
                    Session::set('message', ['type' => "danger", 'title' => 'Error', 'content' => "The database connection failed, try again later"]);
                    return false;
                }
            } else {
                if ($_FILES['file']['error'] === 4) {
                    return true;
                }
                Session::set('message', ['type' => "danger", 'title' => 'Error', 'content' => "The file couldn't be submitted, try again later"]);
                return false;
            }
        }
        Session::set('message', ['type' => "danger", 'title' => 'Error', 'content' => "Something went wrong. Please try a different file"]);
        return false;
    }
}