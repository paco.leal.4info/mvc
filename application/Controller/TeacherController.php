<?php

namespace Mini\Controller;

use Mini\Core\Auth;
use Mini\Core\Session;
use Mini\Model\User;

class TeacherController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkAuth('teacher');
    }

    public function index()
    {
        $this->view->addData(array('title' => 'Teachers'));
        echo $this->view->render('teacher/index');
    }

    public function courses()
    {
        if (isset($_SESSION['user']['grades']) && ($_SESSION['user']['grades'][0])) {
            $courses = $_SESSION['user']['grades'];
            $users = New User();
            $roles = $users->roles();
            $users = $users->course($courses);
            $this->view->addData(array('title' => 'List of Students in your Courses', 'users' => $users, 'roles' => $roles));
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'You have not enrolled in any courses']);
        }
        echo $this->view->render('user/index');
    }

}