<?php


namespace Mini\Controller;

use Mini\Core\Session;
use Mini\Core\Auth;
use Mini\Model\Grade;
use Mini\Model\User;

class GradeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkAuth('teacher');
    }

    public function index()
    {
        $grades = New Grade();
        $grades = $grades->all();
        $this->view->addData(['title' => 'Grades List', 'tmpgrades' => $grades]);
        echo $this->view->render('grade/index');
        return true;
    }

    public function passChange($id)
    {
        $grade = New Grade();
        $grade = $grade->get($id);
        $this->view->addData(array('title' => "Change password from Grade $grade->name", 'grade_id' => $id));
        echo $this->view->render("grade/password");
        return true;
    }

    public function update($id)
    {
        // validar solo enrollment
        $grades = New Grade();
        $grade = $grades->get($id);
        if ($grade->enrollment == $_POST['old_password'] && $_POST['new_password'] == $_POST['enrollment']) {
            if ($grades = $grades->update($id, $_POST['enrollment'])) {
                Session::set('message', ['type' => 'success', 'title' => 'Success', 'content' => "The password for $grade->name was changed"]);
                header( 'Location: /grade/index');
                return true;
            } else {
                Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'The password could not be created']);
                header( 'Location: /grade/index');
                return false;
            }
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'The old or new password was not valid']);
            header( "Location: /grade/passChange/$id");
            return false;
        }

    }

}
