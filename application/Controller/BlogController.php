<?php

namespace Mini\Controller;

use Mini\Model\Post;
use Mini\Model\Grade;
use Mini\Core\Auth;

class BlogController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($page = 1)
    {
        $posts = New Post();
        $maxPage = $posts->getNumberPosts('published');
        $posts = $posts->getPosts($page);
        $this->view->addData(array('title' => 'Blog', 'posts' => $posts, 'maxPage' => $maxPage));
        echo $this->view->render('blog/index');
    }

    public function article($slug)
    {
        $post = New Post();
        $post = $post->getPost($slug);
        $grades = New Grade();
        $tmpgrades = $grades->all();
        $grades = [];
        foreach ($tmpgrades as $tmpgrade) {
            $grades[$tmpgrade->id] = $tmpgrade->name;
        }
        if ($post->status == 'published' || $post->user_id == $_SESSION['user']['id'] || Auth::checkAuth('teacher', false)) {
            $this->view->addData(array('title' => $post->name, 'post' => $post, 'tmpgrades' => $grades));
            echo $this->view->render('blog/article');
        } else {
            header('Location: /error');
        }
    }

}