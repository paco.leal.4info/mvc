<?php

namespace Mini\Controller;

use Mini\Core\Auth;
use Mini\Core\Session;
use Mini\Model\Category;
use Mini\Model\Tag;
use Mini\Model\Grade;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkAuth('admin');
    }

    public function index()
    {
        $this->view->addData(array('title' => 'Web Administration'));
        echo $this->view->render('admin/index');
        return true;
    }

    public function deleteGrade($id)
    {
        $grades = New Grade();
        if ($grades = $grades->delete($id)) {
            Session::set('message', ['type' => 'success', 'title' => 'Success', 'content' => 'The grade was successfully deleted']);
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Failed trying to delete grade']);
        }
        header( 'Location: /grade/index');
        return true;
    }

    public function newGrade()
    {
        $grades = New Grade();
        $grades = $grades->all();
        $this->view->addData(array('title' => 'Create a new Grade', 'tmpgrades' => $grades));
        echo $this->view->render("admin/grade/new");
        return true;
    }

    public function saveGrade()
    {
        if (empty($_POST['parent_id'])) {
            $_POST['parent_id'] = null;
        }
        if (empty($_POST['enrollment'])) {
            $_POST['enrollment'] = null;
        } else {
            if (empty($_POST['parent_id'])) {
                $_POST['parent_id'] = null;
                $_POST['enrollment'] = null;
            }
        }
        $grades = New Grade();
        if ($grades = $grades->create($_POST['name'], $_POST['parent_id'], $_POST['enrollment'])) {
            Session::set('message', ['type' => 'success', 'title' => 'Success', 'content' => 'The new grade was created']);
            header( 'Location: /grade/index');
            return true;
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'The new grade could not be created']);
            header( 'Location: /grade/index');
            return false;
        }

    }

}