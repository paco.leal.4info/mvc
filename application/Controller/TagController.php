<?php

namespace Mini\Controller;

use Mini\Core\Auth;
use Mini\Core\Session;
use Mini\Model\Tag;

class TagController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkAuth('admin');
    }

    public function index()
    {
        $tags = New Tag();
        $tags = $tags->all();
        $this->view->addData(array('title' => 'Tag List', 'tags' => $tags));
        echo $this->view->render('tags/index');
    }

    public function new()
    {
        $this->view->addData(array('title' => "Tag Creation Form", 'route' => 'Tag'));
        echo $this->view->render("tags/new");
    }

    public function create()
    {
        $tags = New Tag();
        if ($tags = $tags->create($_POST['name'])) {
            Session::set('message', ['type' => 'success', 'title' => 'Success', 'content' => 'The new tag was successfully created']);
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Failed trying to create tag']);
        }
        $this->index();
    }

    public function update($id)
    {
        $tags = New Tag();
        if ($tags = $tags->update($id, $_POST['name'])) {
            Session::set('message', ['type' => 'success', 'title' => 'Success', 'content' => 'The tag was successfully updated']);
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Failed trying to update tag']);
        }
        $this->index();
    }

    public function delete($id)
    {
        $tags = New Tag();
        if ($tags = $tags->delete($id)) {
            Session::set('message', ['type' => 'success', 'title' => 'Success', 'content' => 'The tag was successfully deleted']);
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Failed trying to delete tag']);
        }
        $this->index();
    }
}