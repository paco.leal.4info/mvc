<?php

namespace Mini\Controller;

use Mini\Core\Session;
use Mini\Model\Login;

class LoginController extends Controller
{
	public function index()
	{
	    if (isset($_SESSION['user'])) {
            header('Location: /home/welcome');
            return true;
        }
        $this->view->addData(array('title' => 'Login'));
        echo $this->view->render('login/index');
        return true;
	}

	public function doLogin()
    {
        $login = new Login();
        if ($login->doLogin($_POST)) {
            header('Location: /home/welcome');
            return true;
        } else {
            echo $this->view->render('login/index');
            return false;
        }
    }

    public function logout()
    {
        Session::destroy();
        header('Location: /');
        exit();
    }
}

 ?>