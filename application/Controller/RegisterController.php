<?php

namespace Mini\Controller;

use Mini\Core\Session;
use Mini\Model\User;

class RegisterController extends Controller
{
    public function index()
    {
        if (isset($_SESSION['user'])) {
            header('Location: /home/welcome');
            return true;
        }
        $this->view->addData(array('title' => 'Register'));
        echo $this->view->render('register/index');
    }

    public function doRegister()
    {
        $register = new User();
        if ($register->create($_POST)) {
            Session::set('message', ['type' => 'info', 'title' => 'Welcome to AcademiaMVC!', 'content' => 'Your registration was successfull']);
            header('Location: /home/welcome');
        } else {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Something went wrong']);
            echo $this->view->render('register/index');
        }
    }
}

?>