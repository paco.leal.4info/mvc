<?php

/**
 * Class HomeController
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Mini\Controller;

use Mini\Core\Auth;
use Mini\Core\Session;
use Mini\Model\Grade;
use Mini\Model\Post;
use Mini\Model\User;

class HomeController extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {
        $this->view->addData(['title' => 'Home']);
        echo $this->view->render('home/index');
    }

    public function welcome()
    {
        $this->view->addData(['title' => 'Welcome']);
        echo $this->view->render('home/welcome');
    }

    public function about()
    {
        $this->view->addData(['title' => 'About']);
        echo $this->view->render('home/about');
    }

    public function grades($grade_id = null)
    {
        $grade = New Grade();
        $grade = $grade->getAll($grade_id);
        $roots = [];

        if ($grade_id == null) {                                    // si no tiene padre
            $roots['name'] = 'Courses';                                 // el padre es "courses"
            foreach ($grade as $keyroot => $root) {                     // y los hijos los elementos raíz
                $roots[$root->id]['name'] = $root->name;
            }
            $this->view->addData(['title' => 'Grades', 'roots' => $roots]);
            echo $this->view->render('home/grades/nav');
            return true;
        }                                                           // si tiene padre

        foreach ($grade as $keyroot => $root) {                         // por cada raíz
            foreach ($grade as $key1 => $child1) {                      // por cada posible primer hijo
                if ($root->id == $child1->parent_id) {                  // si la raíz tiene hijos
                    $roots['name'] = $root->name;                       // roots se llena con hijos y no entra al if siguiente
                    $roots[$child1->id]['name'] = $child1->name;
                }
            }
        }

        if (empty($roots)) {                                        // si no tiene hijos
            foreach ($grade as $root) {                                 // por cada raíz
                $roots['name'] = $root->name;                           // se guarda el nombre
            }

            Auth::checkAuth('guest');                         // si no ha iniciado sesión redirige a login
            // si ha iniciado sesión
            if (Auth::checkAuth('admin', false)) {                   // y es el administrador
                $this->showgrade($grade_id);                                 // pasamos a showgrade
                return true;
            }                                                       // si no es el administrador

            $usergrades = $_SESSION['user']['grades'];                  // buscamos sus matrículas
            if (empty($usergrades)) {                                   // si no tiene ninguna
                $this->enroll($grade_id);                                   // pasamos a enroll
                return true;
            } else {                                                    // si tiene alguna
                if (in_array($grade_id, $usergrades)) {                      // si el usuario está matriculado en el curso
                    $this->showgrade($grade_id);                                 // pasamos a showgrade
                    return true;
                }                                                      // si se acaban sus matrículas y no está el id que buscamos
                $this->enroll($grade_id);                               // pasamos a enroll
                return true;
            }
        }

        $this->view->addData(['title' => 'Grades', 'roots' => $roots]);
        echo $this->view->render('home/grades/nav');
        return true;
    }

    public function showgrade($grade_id)
    {
        $grades = New Grade();
        $grade = $grades->get($grade_id);
        if (isset($grade->enrollment) && !empty($grade->enrollment)) {
            $posts = $grades->getPosts($grade);
            $this->view->addData(['title' => 'Welcome to ' . ucwords($grade->name), 'tmpgrade' => $grade, 'posts' => $posts]);
            echo $this->view->render('home/grades/show');
            return true;
        }
        $this->view->addData(['title' => ucwords($grade->name) .' is a dead end for now', 'tmpgrade' => $grade_id]);
        echo $this->view->render('home/grades/nocourse');
        return false;
    }

    public function enroll($grade_id)
    {
        $grade = New Grade();
        $grade = $grade->get($grade_id);
        if (isset($grade->enrollment) && !empty($grade->enrollment)) {
            $this->view->addData(['title' => 'Enroll me in ' . ucwords($grade->name), 'tmpgrade' => $grade_id]);
            echo $this->view->render('home/grades/enroll');
            return true;
        }
        $this->view->addData(['title' => ucwords($grade->name) .' is a dead end for now', 'tmpgrade' => $grade_id]);
        echo $this->view->render('home/grades/nocourse');
        return false;
    }

    public function unroll($grade_id)
    {
        Auth::checkAuth('guest');
        $grade = New Grade();
        if ($grade->unroll($grade_id, $_SESSION['user']['id'])) {
            $usergrades = $_SESSION['user']['grades'];
            foreach ($usergrades as $key => $usergrade) {
                $key = array_search($grade_id, $_SESSION['user']['grades']);
                if ($key !== false) {
                    unset($_SESSION['user']['grades'][$key]);
                }
            }
            //mensaje: you unrolled correctly
            header("Location: /home/grades/$grade_id");
            return true;
        }
        //mensaje: something went wrong
        header("Location: /home/grades/$grade_id");
        return false;
    }

    public function singingrade($grade_id)
    {
        if ($_SESSION['user']['role'] == 'teacher' && isset($_SESSION['user']['grades'][0])) {
            Session::set('message', ['type' => 'danger', 'title' => 'Error', 'content' => 'Teachers can not enroll in more than one course']);
            header("Location: /home/grades/$grade_id");
            return false;
        }
        $grades = New Grade();
        $grade = $grades->get($grade_id);
        if ($_POST['password'] == $grade->enrollment) {
            $action = $grades->enroll($_SESSION['user']['id'], $grade_id);
            if ($action) {
                if ($_SESSION['user']['role'] == 'guest') {
                    $user = New User();
                    $updaterole = $user->updateRole($_SESSION['user']['id'], 2);
                    if ($updaterole) {
                        $_SESSION['user']['role'] = 'user';
                    }
                }
                Session::set('user', ['grades' => [$grade_id]]);
                Session::set('message', ['type' => 'success', 'title' => 'You enrolled correctly', 'content' => 'Welcome to your new course. Use this table to explore all the posts available']);
                $this->showgrade($grade->id);
                return true;
            } else {
                Session::set('message', ['type' => 'danger', 'title' => 'Something went wrong', 'content' => 'Please try again later']);
                header('Location: /error');
                return false;
            }
        }
    }

    public function search($what, $where)
    {
        $posts = New Post();
        $posts = $posts->search($what, $where);
        $this->view->addData(array('title' => "Posts with the $what $where", 'posts' => $posts, "$what" => $where));
        echo $this->view->render('blog/index');
        return true;
    }

}
