<?php

namespace Mini\Controller;

use Mini\Core\Auth;
use Mini\Core\Session;
use Mini\Model\User;
use Mini\Model\Message;


class MessageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $messages = New Message();
        $messages = $messages->all($_SESSION['user']['id']);
        $this->view->addData(array('title' => 'Message List', 'messages' => $messages));
        echo $this->view->render('messages/index');
    }

    public function new($id=null)
    {
        Auth::checkAuth('teacher', 'true');
        $users = New User();
        $users = $users->all();
        $this->view->addData(array('title' => 'New Message', 'users' => $users, 'id' => $id));
        echo $this->view->render('messages/form');
    }

    public function create()
    {
        Auth::checkAuth('teacher', 'true');
        $users = $_POST['users'];
        unset($_POST['users']);
        $message = New Message();

        if ($id = $message->create($_POST)) {
            $message->linkUsers($id, $users);
        } else {
            Session::set('message', array('type' => 'danger', 'title' => 'Error', 'content' => 'There was an unexpected error'));
            return false;
        }
        Session::set('message', array('type' => 'success', 'title' => 'The message was sent', 'content' => 'It may take a few minutes to display'));
        $this->view->addData(array('title' => 'New Message'));
        header('Location: /user');
    }

    public function update($id)
    {
        if ($_POST['role_id'] < 2 || $_POST['role_id'] > 4) {
            header('Location: /error');
        }
        // fin de las validaciones
        $user = New User();
        $user = $user->update($id, $_POST);
        $this->index();
    }

    public function delete($id)
    {
        $users = New User();
        $users = $users->delete($id);
        $this->index();
    }

    public function show($id)
    {
        $message = New Message();
        if ($read = $message->read($id)) {
            $unread = $message->unread();
            $message = $message->get($id);
            $this->view->addData(array('title' => $message->subject, 'message' => $message, 'unread' => $unread));
            echo $this->view->render('messages/show');
        }
    }
}