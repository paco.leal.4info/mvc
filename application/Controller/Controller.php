<?php

namespace Mini\Controller;
use Mini\Core\Session;
use Mini\Core\Template;
use Mini\Model\Message;
use Mini\Model\Grade;

class Controller
{
    public function __construct()
    {
        Session::init();
        $unread = new Message();
        $unread = $unread->unread();
        $grades = New Grade();
        $grades = $grades->all();

        $this->view = Template::templates();
        $this->view->addData(array('title' => 'AcademiaMVC', 'unread' => $unread, 'grades' => $grades));
    }
}