<?php

namespace Mini\Core;

class Session
{
    public static function init()
    {
        if (session_id() == '') {
            session_start();
        }
    }

    public static function set($clave, $params)
    {
        foreach ($params as $key => $value) {
            $_SESSION[$clave][$key] = $value;
        }
    }

    public static function get($clave1, $clave2 = null)
    {
        switch ($clave2) {
            case null:
                return $_SESSION[$clave1];
                break;
            default:
                return $_SESSION[$clave1][$clave2];
                break;
        }
    }

    public static function add($params)
    {
        foreach ($params as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }

    public static function delete($clave1, $clave2 = null)
    {
        switch ($clave2) {
            case null:
                unset($_SESSION[$clave1]);
                break;
            default:
                unset($_SESSION[$clave1][$clave2]);
                break;
        }
    }

    public static function destroy()
    {
        session_destroy();
    }
}