<?php

namespace Mini\Core;

class Auth
{
    public static function checkAuth($role, $redirect = true)
    {
        $niveles = array('guest' => 0, 'user' => 1, 'teacher' => 2, 'admin' => 3);
        Session::init();
        if (!isset($_SESSION['user']['role'])) {
            Session::destroy();
            if ($redirect) {
                header('Location: /login');
            }
            exit();
        }

        if ($niveles[Session::get('user', 'role')] >= $niveles[$role]) {
            return true;
        }

        if ($redirect) {
            header('Location: /error');
        }
        return false;

    }


}