<?php

namespace Mini\Core;
use League\Plates\Engine;

class Template
{
    private static $templates;

    public static function templates()
    {
        if (!Template::$templates) {
            Template::$templates = new Engine(APP . 'view');
            Template::$templates->addData(['titulo' => 'Academia']);
            // Implementar función de mensajes
        }

        return Template::$templates;
    }
}