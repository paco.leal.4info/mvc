<?php

namespace Mini\Core;
use Mini\Model\Message;

class Functions
{
    public static function slug($string)
    {
        $temporal = str_replace(' ','-', strtolower($string));
        $slug = rtrim($temporal, '.');
        return $slug;
    }

    public static function recTree($grades=null, $roots=null)
    {/*
        if (empty($roots)) {
            foreach ($grades as $grade) {                                                  // por cada nivel1
                if ($grade->parent_id == null) {                                        // si no tiene padre
                    $roots[$grade->id]['name'] = $grade->name;                         // guardamos en raíz
                } else {                                                           // si tiene nivel2
                    foreach ($grades as $level1) {                                         // por cada curso
                        if ($grade->id == $level1->parent_id) {                                       // si el id del nivel2 coincide con el del padre
                            $roots[$level1->parent_id][$level1->id]['name'] = $grade->name;
                        }
                    }
                    if (count($grade) > 1) {
                        self::recTree($grades, $roots[$grade->id]);
                    }
                }
            }
        } else {
            foreach ($roots as $key => $grade) {
                foreach ($grade as $level1) {
                    if ($key == $level1->parent_id) {                                       // si el id del nivel2 coincide con el del padre
                        $roots[$level1->parent_id][$level1->id]['name'] = $grade->name;
                        unset($grades[$level1->id]);
                    }
                }
            }
            if (count($grade) > 1) {
                self::recTree($grades, $roots[$grade->id]);
            }
        }

        return $roots;*/
        return true;
    }

    public static function tree($grades)
    {
        if (empty($grades)) {
            return;
        }
        foreach ($grades as $grade) {                                               //por cada curso
            if ($grade->parent_id == null) {                                        // si no tiene padre
                $raices[$grade->id]['name'] = $grade->name;                         // guardamos en raíz
            } else {                                                                // y si tiene padre
                if (isset($raices[$grade->parent_id]['name'])) {                    // si el padre existe en el array
                    $raices[$grade->parent_id][$grade->id]['name'] = $grade->name;  // guardamos en el nivel1
                }
            }
        }

        foreach ($raices as $keyroot => $raiz) {                                     // por cada raíz
            foreach ($raiz as $key1 => $nivel1) {                                    // por cada nivel1
                foreach ($grades as $grade) {                                        // por cada curso
                    if ($key1 == $grade->parent_id) {                                // si el id del nivel1 coincide con el del padre
                        $raices[$keyroot][$key1][$grade->id]['name'] = $grade->name; // guardamos en el nivel2
                    }
                }
            }
        }

        foreach ($raices as $keyroot => $raiz) {                                                    // por cada raíz
            foreach ($raiz as $key1 => $nivel1) {                                                   // por cada nivel1
                if (count($nivel1) > 1) {                                                           // si tiene nivel2
                    foreach ($nivel1 as $key2 => $nivel2) {                                         // por cada nivel2
                        foreach ($grades as $grade) {                                               // por cada curso
                            if ($key2 == $grade->parent_id) {                                       // si el id del nivel2 coincide con el del padre
                                $raices[$keyroot][$key1][$key2][$grade->id]['name'] = $grade->name; // guardamos en el nivel3
                            }
                        }
                    }
                }
            }
        }

        return $raices;
    }

    public static function dropdown($roots)
    {
        $sidearrow = '&#xf0da;';
        $dropdown = '';
            if (!empty($roots)) {
                foreach ($roots as $keyroot => $root):
                    $dropdown .= "<li><a href=\"/home/grades/$keyroot\" class=\"text-uppercase\">$root[name]<i class=\"fa\" style=\"font-size:18px; margin-left: 0.2em;\">$sidearrow</i></a>";
                         $dropdown .= "<ul class=\"list-unstyled\">";
                            foreach ($root as $key1 => $level1):
                                if (isset($level1['name'])):
                                    $dropdown .= "<li><a href=\"/home/grades/$key1\" class=\"text-uppercase\">$level1[name]";
                                    if (count($level1) > 1) {
                                        $dropdown .= "<i class=\"fa\" style=\"font-size:18px; margin-left: 0.2em;\">$sidearrow</i>";
                                    }
                                    $dropdown .= "</a>";
                                        $dropdown .= "<ul class=\"list-unstyled\">";
                                            foreach ($level1 as $key2 => $level2):
                                                if (isset($level2['name'])):
                                                    $dropdown .= "<li><a tabindex=\"-1\" href=\"/home/grades/$key2\" class=\"text-uppercase\">$level2[name]";
                                                    if (count($level2) > 1) {
                                                        $dropdown .= "<i class=\"fa\" style=\"font-size:18px; margin-left: 0.2em;\">$sidearrow</i>";
                                                    }
                                                    $dropdown .= "</a>";
                                                    $dropdown .= "<ul class=\"list-unstyled\">";
                                                    foreach ($level2 as $key3 => $level3):
                                                        if (isset($level3['name'])):
                                                            $dropdown .= "<li><a tabindex=\"-1\" href=\"/home/grades/$key3\" class=\"text-uppercase\">$level3[name]";
                                                            if (count($level3) > 1) {
                                                                $dropdown .= "<i class=\"fa\" style=\"font-size:18px; margin-left: 0.2em;\">$sidearrow</i>";
                                                            }
                                                            $dropdown .= "</a></li>";
                                                        endif;
                                                    endforeach;
                                                    $dropdown .= "</ul>";
                                                    $dropdown .= "</li>";
                                                endif;
                                            endforeach;
                                        $dropdown .= "</ul>";
                                    $dropdown .= "</li>";
                                endif;
                            endforeach;
                        $dropdown .= "</ul>";
                    $dropdown .= "</li>";
                endforeach;
            } else {
                $dropdown .= '<li class="text-uppercase"><a href="#">There are no courses available</a></li>';
            }
        return $dropdown;
    }

    public static function indexAdmin()
    {
        $messages = New Message();
        $messages = $messages->all($_SESSION['user']['id']);
        return $messages;
    }

    public static function visible($post, $tmpgrades, $tags)
    {
        $access = false;
        if (isset($_SESSION['user']['grades'])) {
            foreach ($_SESSION['user']['grades'] as $course) {
                foreach ($tags as $tag) {
                    if ($tag == str_replace(' ', '-', strtolower($tmpgrades[$course]))) {
                        $access = true;
                    }
                }
            }
        }

        return $access;
    }

    public static function dropdownAdmin($roots)
    {
        $sidearrow = '&#xf0d9;';
        $dropdown = '';
        if (!empty($roots)) {
            foreach ($roots as $keyroot => $root):
                $dropdown .= "<li class=\"divider\"></li>";
                $dropdown .= "<li><a ";
                $dropdown .= "href=\"/home/grades/$keyroot\"";
                $dropdown .= ">$root[name]<span class='caret'></span></a>";
                foreach ($root as $key1 => $level1):
                    if (isset($level1['name'])):
                        $dropdown .= "<li style='margin-left: 1em;'><a ";
                        $dropdown .= "href=\"/home/grades/$key1\"";
                        $dropdown .= ">$level1[name]";
                        if (count($level1) > 1) {
                            $dropdown .= "<span class='caret'></span>";
                        }
                        $dropdown .= "</a>";

                        foreach ($level1 as $key2 => $level2):
                            if (isset($level2['name'])):
                                $dropdown .= "<li style='margin-left: 2em;'><a tabindex=\"-1\" href=\"/home/grades/$key2\"";
                                $dropdown .= "href=\"/home/grades/$key2\"";
                                $dropdown .= ">$level2[name]";
                                if (count($level2) > 1) {
                                    $dropdown .= "<span class='caret'></span>";
                                }
                                $dropdown .= "</a>";

                                foreach ($level2 as $key3 => $level3):
                                    if (isset($level3['name'])):
                                        $dropdown .= "<li style='margin-left: 3em;'><a tabindex=\"-1\" href=\"/home/grades/$key3\"";
                                        $dropdown .= "href=\"/home/grades/$key3\"";
                                        $dropdown .= ">$level3[name]";
                                        if (count($level3) > 1) {
                                            $dropdown .= "<span class='caret'></span>";
                                        }
                                        $dropdown .= "</a></li>";
                                    endif;
                                endforeach;
                                $dropdown .= "</li>";
                            endif;
                        endforeach;
                        $dropdown .= "</li>";
                    endif;
                endforeach;
                $dropdown .= "</li>";
            endforeach;
        } else {
            $dropdown .= '<li class="dropdown-toggle"><a href="#">There are no courses available</a></li>';
            $notrim = true;
        }
        if (!isset($notrim)) {
            $dropdown = substr($dropdown, 25);
            //ltrim($dropdown, "<li class=\"divider\"></li>");
        }
        return $dropdown;
    }

}
