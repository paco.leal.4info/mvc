<?= $this->layout('layouts/publiclayout'); ?>
<section class="pages container">
    <div class="page page-about">
        <?php if (!isset($_SESSION['user'])): ?>
            <h1 class="text-capitalize">This is embarrasing</h1>
            <cite>Error 15960473: "The page you are trying to access doesn't exist"</cite>
            <div class="divider-2" style="margin: 35px 0;"></div>
            <p>This is the Error-page. You may have done something wrong, you can come back by clicking <a href="/">here</a>.</p>
        <?php elseif ($_SESSION['user']['role'] == 'guest'): ?>
            <h1 class="text-capitalize">Wooops, you shouldn't see this</h1>
            <cite>Error 15960472: "This is not the page you're looking for"</cite>
            <div class="divider-2" style="margin: 35px 0;"></div>
            <p>This is the Error-page. If you just registered, you should wait for the admins to change your role. Or maybe you just tried to write random things on the bar.</p>
            <p>You can come back by clicking <a href="/">here</a>. Be careful where you go, and don't mess too much with the bar, please... you could break something!</p>
        <?php elseif ($_SESSION['user']['role'] == 'user'): ?>
            <h1 class="text-capitalize">Mmmm... this is not what you expected</h1>
            <cite>Error 15960472: "The page you are trying to access doesn't exist"</cite>
            <div class="divider-2" style="margin: 35px 0;"></div>
            <p>This is the Error-page. Maybe you tried to access something only for teachers? Or maybe you just tried to write random things on the bar.</p>
            <p>Anyway, you can come back by clicking <a href="/">here</a>. Be careful where you go, and don't mess too much with the bar, please... you could break something!</p>
        <?php elseif ($_SESSION['user']['role'] == 'teacher'): ?>
            <h1 class="text-capitalize">Oh my, what did you just do?</h1>
            <cite>Error 15960471: "This isn't the right page, you just broke it"</cite>
            <div class="divider-2" style="margin: 35px 0;"></div>
            <p>This is the Error-page. Please be careful when writing on the bar. You can come back by clicking <a href="/">here</a>.</p>
            <p>Oh, and remember you are a teacher, don't do anything reckless.</p>
        <?php elseif ($_SESSION['user']['role'] == 'admin'): ?>
            <h1 class="text-capitalize">It's over admin, I have the high ground</h1>
            <cite>Error 0504-B-W-U: "This is where the fun begins"</cite>
            <div class="divider-2" style="margin: 35px 0;"></div>
            <p>This is the Error-page. What the hell did you just do? Why can't you be like those guys who just do what they should? Do you think I'm hiding something?</p>
            <p>Oh, so now I'm hiding something. This page was done by a student so there must be some errors here and there, you say... what? a Controller, you say?</p>
        <?php endif; ?>
    </div>
</section>