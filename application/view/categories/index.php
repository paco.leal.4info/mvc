<?= $this->layout('layouts/adminlayout');
$section = explode('/', ($_SERVER['REQUEST_URI']));
$section = end($section);
$_SESSION['section'] = $section;
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= $title ?></h3>
                    <td><a href="/category/new" class="btn btn-default pull-right"><i class="fa fa-plus-circle"></i> New</a></td>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if (isset($categories) && !empty($categories)): ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Category <i class="fa fa-pencil"></i></th>
                                <th style="width: 10%;" class="no-sort">Update</th>
                                <th style="width: 10%;" class="no-sort">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($categories as $category): ?>
                                <tr>
                                    <td><?= $category->id ?></td>
                                    <form action="/category/update/<?= $category->id ?>" method="POST">
                                        <td><input type="text" name="name" style="border:0; background-color: transparent;" value="<?= $category->name ?>"><span hidden><?= $category->name ?></span></td>
                                        <td><button class="btn btn-sm btn-info btn-block"><i class="fa fa-floppy-o"></i> Save</button></td>
                                    </form>
                                    <td><a href="/category/delete/<?= $category->id ?>" class="btn btn-sm btn-danger btn-block" onClick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</span></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <h2 class="text-secondary">There are no Categories yet</h2>
                    <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>