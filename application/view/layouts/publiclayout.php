<?php
$section = explode('/', ($_SERVER['REQUEST_URI']));
?>
<!---->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?= (isset($title))? $title : 'AcademiaMVC' ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <?= (in_array('message', $section))? "<link rel='stylesheet' href='" . URL . "bower_components/bootstrap/dist/css/bootstrap.min.css'>" : '<link rel="stylesheet" href="' . URL . 'css/datatables.css">' ?>
    <link rel="stylesheet" href="<?= URL; ?>css/normalize.css">
    <link rel="stylesheet" href="<?= URL; ?>css/framework.css">
    <link rel="stylesheet" href="<?= URL; ?>css/style.css">
    <link rel="stylesheet" href="<?= URL; ?>css/responsive.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= URL; ?>bower_components/font-awesome/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= URL; ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
</head>
<body>
<div class="preload"></div>
<header class="space-inter">
    <div class="container container-flex space-between">
        <a href="/"><figure class="logo"><img src="<?= URL; ?>img/logo2.png" style="height: 2.3em; width: auto;" alt=""></figure></a>
        <?= $this->insert('partials/navbar') ?>
    </div>
</header>
<?= $this->insert('partials/message') ?>
<?= $this->section('content') ?>

<?= '' //$this->insert('_templates/footer') ?>
<!---->
<section class="footer">
    <footer>
        <a href="#" id="back" class="float"><i class="fa fa-arrow-left"></i></a>
        <div class="container">
            <figure class="logo"><img src="<?= URL; ?>img/logo2.png" style="height: 2.3em; width: auto;" alt=""></figure>
            <nav>
                <ul class="container-flex space-center list-unstyled">
                    <li><a href="/" class="text-uppercase c-white">home</a></li>
                    <li><a href="/home/about" class="text-uppercase c-white">about</a></li>
                </ul>
            </nav>
            <p>Based on © 2017 - Zendero. All Rights Reserved. Designed & Developed by <span class="c-white">Agencia De La Web</span></p>
            <div class="divider-2" style="width: 80%;"></div>
            <ul class="social-media-footer container-flex space-between list-unstyled">
                <li><a href="https://www.facebook.com" class="fa fa-facebook-official c-white" style="font-size: 2em;"></a></li>
                <li><a href="https://www.twitter.com" class="fa fa-twitter c-white" style="font-size: 2em;"></a></li>
                <li><a href="https://www.instagram.com" class="fa fa-instagram c-white" style="font-size: 2em;"></a></li>
                <li><a href="https://www.pinterest.com" class="fa fa-pinterest c-white" style="font-size: 2em;"></a></li>
            </ul>
        </div>
    </footer>
</section>
<!-- jQuery 3 -->
<script src="<?= URL; ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= URL; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src='<?= URL ?>bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
<script src='<?= URL ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>
<script>
    $(document).ready(function () {
        $('#back').click(function() {
            parent.history.back();
            return false;
        });

        $('#example1').DataTable({
            'columnDefs': [
                { 'orderable': false, 'targets': 'no-sort' }
            ],
            'order': [[0, 'desc']]
        });

    });
</script>
</body>
</html>