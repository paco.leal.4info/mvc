<?= $this->layout('layouts/publiclayout'); ?>
<section class="posts container">
<?php if (isset($posts) && !empty($posts)):
    foreach ($posts as $post): ?>
        <article class="post w-image">
            <figure><img src="<?= $post->file ?>" alt="" class="img-responsive"></figure>
            <div class="content-post">
                <header class="container-flex space-between">
                    <div class="date">
                        <span class="c-gray-1"><?= $post->fecha ?></span>
                    </div>
                    <div class="post-category">
                        <span class="category text-capitalize"><a href="/home/search//category/<?= $post->category ?>" class="c-white" style="font-weight: normal; font-size: 100%;"><?= $post->category ?></a></span>
                    </div>
                </header>
                <h1><?= $post->name ?></h1>
                <div class="divider"></div>
                <cite class="cite"><?= $post->excerpt ?></cite>
                <footer class="container-flex space-between">
                    <div class="read-more">
                        <a href="/blog/article/<?= $post->slug ?>" class="text-uppercase c-green">read more</a>
                    </div>
                    <div class="tags container-flex">
                        <?php $tags = explode(',', $post->etiquetas);
                        foreach ($tags as $tag): ?>
                            <span><a href="/home/search/tag/<?= $tag ?>" class="tag c-gris custom-tag" style="font-size: 110%;">#<?= $tag ?></a></span>
                        <?php endforeach; ?>
                    </div>
                </footer>
            </div>
    </article>
    <?php endforeach; ?>
    </section>
<?php
    if (isset($maxPage)) {
        $pagination = 5;                                            // change here and Post Model to change pagination
        $page = explode('/',$_SERVER['REQUEST_URI']);
        $page = end($page);
        if (!is_numeric($page)) {
            $page = 1;
        }
    ?>
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="pagination">
                    <ul class="list-unstyled container-flex space-center">
                        <?= ($page > 2)? '<li><a href="/blog/index/' . ($page - 2) . '">' . ($page - 2) . '</a></li>': ''; ?>
                        <?= ($page > 1)? '<li><a href="/blog/index/' . ($page - 1) . '">' . ($page - 1) . '</a></li>': ''; ?>
                        <li><a href="/blog/index/<?= $page ?>" class="pagination-active"><?= $page ?></a></li>
                        <?= ($page < (ceil($maxPage/$pagination)))? '<li><a href="/blog/index/' . ($page + 1) . '">' . ($page + 1) . '</a></li>': ''; ?>
                        <?= ($page < (ceil($maxPage/$pagination) - 1))? '<li><a href="/blog/index/' . ($page + 2) . '">' . ($page + 2) . '</a></li>': ''; ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php
    }
?>
<?php else: ?>
    <article class="post w-image">
            <div class="content-post">
                <cite>There are no Posts to list</cite>
            </div>
        </article>
    </section>
<?php endif; ?>
