<?= $this->layout('layouts/publiclayout');
use Mini\Core\Functions;
use Mini\Core\Auth;

$tags = explode(',', $post->etiquetas);
?>
<article class="post image-w-text container">
    <div class="content-post ">
        <header class="container-flex space-between">
            <div class="date">
                <span class="c-gris"><?= $post->fecha ?></span>
            </div>
            <div class="post-category">
                <span class="category"><a href="/home/search/category/<?= $post->category ?>" class="c-white" style="font-weight: normal; font-size: 100%;"><?= $post->category ?></a></span>
            </div>
        </header>
        <h1><?= $post->name ?></h1>
        <div class="divider"></div>
        <div class="image-w-text">
            <figure class="block-left" id="img-margin"><img src="<?= $post->file ?>" alt="" class="img-responsive"></figure>
            <?php if ($post->access == 'public'): ?>
            <div class="text-justify">
                <?= $post->body ?>
            </div>
            <?php else:
                if (Auth::checkAuth('teacher', 'false' || Functions::visible($post, $tmpgrades, $tags))):?>
                    <div class="text-justify">
                        <?= $post->body ?>
                    </div>
                <?php else: ?>
                    <div class="text-center">
                        <cite>To read this post, please enroll in the course</cite>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <footer class="container-flex space-between">
            <div class="tags container-flex">
                <?php foreach ($tags as $tag): ?>
                    <span><a href="/home/search/tag/<?= $tag ?>" class="tag c-gris custom-tag" style="font-size: 110%;">#<?= $tag ?></a></span>
                <?php endforeach; ?>
            </div>
        </footer>
        <div class="divider-2" style="margin: 35px 0;"></div>
        <div id="disqus_thread"></div>
        <script>

            /**
             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
            /*
            var disqus_config = function () {
            this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
            this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
            };
            */
            (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://mvc-test.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    </div>
</article>
