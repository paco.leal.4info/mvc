<?= $this->layout('layouts/publiclayout'); ?>

<section class="pages container">
    <div class="page page-contact">
        <h1 class="text-capitalize">Login</h1>
        <div class="divider-2" style="margin:25px 0;"></div>
        <div class="form-contact">
            <form action="/login/dologin" method="POST">
                <div class="input-container container-flex space-between">
                    <input type="text" name="email" placeholder="Email" class="input-email">
                    <input type="password" name="password" placeholder="Password" class="input-password">
                </div>
                    <input type="submit" value="Login" class="text-uppercase c-green">
            </form>
        </div>
    </div>
</section>