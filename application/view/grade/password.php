<?= $this->layout('layouts/adminlayout'); ?>
<div class="col-xs-7" style="margin-top: 1em;">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <form action="/grade/update/<?= $grade_id ?>" class="form-horizontal" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Old Password</label>

                    <div class="col-sm-10">
                        <input class="form-control" name="old_password" id="password" placeholder="Password" type="password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">New Password</label>

                    <div class="col-sm-10">
                        <input class="form-control" name="new_password" id="password" placeholder="Password" type="password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Repeat New Password</label>

                    <div class="col-sm-10">
                        <input class="form-control" name="enrollment" id="password" placeholder="Password" type="password">
                    </div>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <a href="/grade" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-success pull-right">Save</button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
</div>
