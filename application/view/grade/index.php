<?= $this->layout('layouts/adminlayout');
use Mini\Core\Auth;?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= $title ?></h3>
                    <?php if (Auth::checkAuth('admin', false)): ?>
                        <td><a href="/admin/newGrade" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> New</a></td>
                    <?php endif; ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if (isset($tmpgrades) && !empty($tmpgrades)): ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="width: 10%;">Created</th>
                                <th>Grade/Course</th>
                                <th style="width: 15%;">Parent</th>
                                <th style="width: 8%;" class="no-sort">Preview</th>
                                <th style="width: 8%;" class="no-sort">Password</th>
                                <th style="width: 8%;" class="no-sort">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($tmpgrades as $tmpgrade): ?>
                            <tr>
                                <td><?= $tmpgrade->created_at ?></td>
                                <td><?= $tmpgrade->name ?></td>
                                <td>
                                    <?php if (isset($tmpgrade->parent_id)) {
                                    foreach ($tmpgrades as $parent) {
                                    if ($tmpgrade->parent_id == $parent->id) {
                                    echo $parent->name;
                                    }
                                    }
                                    } else {
                                    echo '<p style="color:red;">none</p>';
                                    } ?>
                                </td>
                                <td><a href="/home/grades/<?= $tmpgrade->id ?>" class="btn btn-sm btn-info btn-block"><i class="fa fa-external-link"></i> Go to Grade</a></td>
                                <td>
                                    <?php if ((Auth::checkAuth('admin', false) || (in_array($tmpgrade->id, $_SESSION['user']['grades']) && Auth::checkAuth('teacher', false))) && isset($tmpgrade->enrollment)): ?>
                                    <a href="/grade/passChange/<?= $tmpgrade->id ?>" class="btn btn-sm btn-warning btn-block"><i class="fa fa-lock"></i> Change</span></a>
                                    <?php else: ?>
                                        <p class="text-center" style="color:grey; font-weight: bold; font-size: 1.5em; margin:0; padding:0;"><i class="fa fa-ban"></i></span></p>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if (Auth::checkAuth('admin', false)):?>
                                    <a href="/admin/deleteGrade/<?= $tmpgrade->id ?>" class="btn btn-sm btn-danger btn-block"  onClick="return confirm('Are you sure?\n(Any child grade will be deleted too)')"><i class="fa fa-trash"></i> Delete</span></a>
                                    <?php else: ?>
                                        <p class="text-center" style="color:grey; font-weight: bold; font-size: 1.5em; margin:0; padding:0;"><i class="fa fa-ban"></i></span></p>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <h2 class="text-secondary">There are no Grades yet</h2>
                    <?php endif; ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
