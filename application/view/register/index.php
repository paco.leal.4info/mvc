<?= $this->layout('layouts/publiclayout'); ?>

<section class="pages container">
    <div class="page page-contact">
        <h1 class="text-capitalize">Sign In</h1>
        <div class="divider-2" style="margin:25px 0;"></div>
        <div class="form-contact">
            <form action="/register/doRegister" method="POST">
                <div class="input-container container-flex space-between">
                    <input type="text" name="name" placeholder="Name" class="input-text">
                    <input type="text" name="email" placeholder="Email" class="input-email">
                    <input type="password" name="password1" placeholder="Password" class="input-password">
                    <input type="password" name="password2" placeholder="Repeat your password" class="input-password">
                </div>
                <input type="submit" value="Register" class="text-uppercase c-green">
            </form>
        </div>
    </div>
</section>