<?= $this->layout('layouts/adminlayout'); ?>
<form action='/message/create' method="POST" enctype="multipart/form-data">
    <!-- Subject -->
    <div class="form-group col-xs-10" style="margin-left: -1em;">
        <label>Subject</label>
        <input class="form-control" name="subject" placeholder="Enter subject" type="text" value="<?= (isset($post))? "$message->subject" : '' ?>">
    </div>
    <!-- Addressees -->
    <div class="form-group col-xs-2 pull-right" style="margin-right: -1em;">
        <label>Addressees</label>
        <select class="form-control select2 select2-hidden-accessible" multiple="" name="users[]" data-placeholder="Select addressees" style="width: 100%;" tabindex="-1" aria-hidden="true">
            <?php if (isset($message)) {
                $message->users_id = explode(',', $message->users_id);
            }
            foreach ($users as $user): ?>
                <option <?= ((isset($message) && (in_array($user->id, $message->users_id))) || ($user->id == $id))? 'selected' : '' ?> value="<?= $user->id ?>"><?= $user->name ?></option>
            <?php endforeach; ?>
        </select>
    </div><br>
    <!-- CKEditor -->
    <div class="form-group" style="margin-top: 4em;">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">CK Editor
                    <small>Advanced and full of features</small>
                </h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
                <!-- /. tools -->
            </div>
            <!-- /.box-header  style="visibility: hidden; display: none;"-->
            <div class="box-body pad">
                <textarea id="body" name="content" rows="10" cols="80" placeholder="Write your message"><?= (isset($message))? "$message->content" : '' ?></textarea>
            </div>
        </div>
        <div class="justify-content-around">
            <input type="submit" class="btn btn-success pull-right" value="Save">
        </div>
    </div>
</form>