<?= $this->layout('layouts/publiclayout');
use Mini\Core\Auth;
?>

<article class="page container">
    <div class="col-xs-12">
        <div>
            <h1><?= $titulo ?> <?= (Auth::checkAuth('teacher', false))? '<span><a href="/message/new" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> New</a></span>' : '' ?></h1>
        </div>
        <div class="box">
            <div class="box-body">
                <?php if (isset($messages) && !empty($messages)): ?>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th style="width: 20%;">Date</th>
                            <th style="width: 15%;">From</th>
                            <th>Subject</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($messages as $message): ?>
                            <tr>
                                <td><?= $message->date ?></td>
                                <td><?= $message->author ?></td>
                                <td><a href="/message/show/<?= $message->id ?>" style="<?= ($message->status == 'read')? 'color: #551A8B;' : '' ?>"><?= $message->subject ?> <i class="fa fa-external-link"></i></a><span hidden><?= $message->subject ?></span><?= ($message->status == 'read')? '<i class="fa fa-check pull-right"></i>' : '' ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <h3>You have not recieved any message</h3>
                <?php endif; ?>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</article>