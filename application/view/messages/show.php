<?= $this->layout('layouts/publiclayout'); ?>

<article class="page container">
    <div class="col-xs-12">
        <div>
            <h1 class="text-capitalize"><?= $message->author ?><span style="font-size: 50%;" class="pull-right"><?= $message->date ?></span></h1>
            <cite><?= $message->subject ?></cite>
            <div class="divider-2" style="margin: 35px 0;"></div>
            <p><?= $message->content ?></p>
        </div>
        <div class="divider-2" style="margin: 35px 0;"></div>
        <div id="disqus_thread"></div>
        <script>

            /**
             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
            /*
            var disqus_config = function () {
            this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
            this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
            };
            */
            (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://mvc-test.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    </div>
    <!-- /.col -->
</article>