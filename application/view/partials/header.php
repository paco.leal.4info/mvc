<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?= $titulo ?></title>
    <link rel="stylesheet" href="<?= URL; ?>css/normalize.css">
    <link rel="stylesheet" href="<?= URL; ?>css/framework.css">
    <link rel="stylesheet" href="<?= URL; ?>css/style.css">
    <link rel="stylesheet" href="<?= URL; ?>css/responsive.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= URL; ?>bower_components/font-awesome/css/font-awesome.min.css">
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
</head>
<body>
<div class="preload"></div>
<header class="space-inter">
    <div class="container container-flex space-between">
        <figure class="logo"><img src="<?= URL; ?>img/logo.png" alt=""></figure>
        <?= $this->insert('_templates/navbar') ?>
    </div>
</header>