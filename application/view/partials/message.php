<?php use Mini\Core\Session;

if (isset($_SESSION['message'])) {
    $message = Session::get('message'); ?>
<section class="posts container">
    <div class="post no-image bg-<?= $message['type'] ?>">
        <h3><?= $message['title'] ?></h3>
        <?= $message['content'] ?>
    </div>
</section>
    <?php Session::delete('message');
} ?>