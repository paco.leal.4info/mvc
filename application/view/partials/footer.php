    <footer>
        <div class="container">
            <figure class="logo"><img src="<?= URL; ?>img/logo.png" alt=""></figure>
            <nav>
                <ul class="container-flex space-center list-unstyled">
                    <li><a href="/" class="text-uppercase c-white">home</a></li>
                    <li><a href="about.html" class="text-uppercase c-white">about</a></li>
                    <li><a href="contact.html" class="text-uppercase c-white">contact</a></li>
                </ul>
            </nav>
            <!--
            <div class="divider-2"></div>
            <p>Nunc placerat dolor at lectus hendrerit dignissim. Ut tortor sem, consectetur nec hendrerit ut, ullamcorper ac odio. Donec viverra ligula at quam tincidunt imperdiet. Nulla mattis tincidunt auctor.</p>
            <div class="divider-2" style="width: 80%;"></div>
            -->
            <p>© 2017 - Zendero. All Rights Reserved. Designed & Developed by <span class="c-white">Agencia De La Web</span></p>
            <div class="divider-2" style="width: 80%;"></div>
            <ul class="social-media-footer container-flex space-between list-unstyled">
                <li><a href="https://www.facebook.com" class="fa fa-facebook-official c-white" style="font-size: 2em;"></a></li>
                <li><a href="https://www.twitter.com" class="fa fa-twitter c-white" style="font-size: 2em;"></a></li>
                <li><a href="https://www.instagram.com" class="fa fa-instagram c-white" style="font-size: 2em;"></a></li>
                <li><a href="https://www.pinterest.com" class="fa fa-pinterest c-white" style="font-size: 2em;"></a></li>
            </ul>
        </div>
    </footer>
</section>
</body>
</html>