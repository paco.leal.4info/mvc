<?php
use Mini\Core\Auth;
use Mini\Core\Functions;

$roots =  Functions::tree($grades);
$downarrow = '&#xf0d7;';
?>

<nav class="custom-wrapper" id="menu" style="overflow: visible">
    <div class="pure-menu"></div>
    <ul class="container-flex list-unstyled">
        <li><a href="/home/grades" class="text-uppercase" >Courses <i class="fa" style="font-size:18px; margin-left: 0.2em;"><?= $downarrow ?></i></a>
            <ul class="list-unstyled">
                <?php if (isset($roots)) {
                    echo Functions::dropdown($roots);
                } else { ?>
                    <li class="text-uppercase"><a href="#">There are no courses available</a></li>
                <?php } ?>
            </ul>
        </li>
        <li><a href="/blog" class="text-uppercase">Blog</a></li>
        <?php if (!isset($_SESSION['user'])): ?>
            <li><a href="/login" class="text-uppercase"><span class="fa fa-sign-in" style="margin-right: 0.5em;"></span>Login</a></li>
            <li><a href="/register" class="text-uppercase"><span class="fa fa-user-plus" style="margin-right: 0.5em;"></span>Sign In</a></li>
        <?php else:
            if (Auth::checkAuth('teacher', false) && $_SESSION['user']['role'] == 'teacher'):?>
                <li><a href="/teacher" class="text-uppercase">Teachers</a></li>
            <?php endif; ?>
            <?php if (Auth::checkAuth('admin', false)):?>
                <li><a href="/admin" class="text-uppercase">Admin</a></li>
            <?php endif; ?>
            <li><a href="/message" <?= (isset($unread) && !empty($unread))? 'style="padding-right: 0;"': '' ?>>
                    <?php if (isset($unread) && !empty($unread)): ?>
                        <i class="fa fa-bell"></i>
                        <span class="label label-info"><?= $unread ?></span>
                    <?php else: ?>
                        <i class="fa fa-bell-o" style="font-weight: bolder;"></i>
                    <?php endif; ?>
                </a>
            </li>
            <li><a href="/login/logout" class="text-uppercase">Logout<span class="fa fa-sign-out" style="margin-left: 0.5em;"></span></a></li>
        <?php endif; ?>
    </ul>
</nav>