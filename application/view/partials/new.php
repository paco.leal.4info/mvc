<div class="col-md-5">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Create new <?= $route ?></h3>
        </div>
        <form class="form-horizontal" action="<?= "/$route/create" ?>" method="POST">
            <div class="box-body">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><?= $route ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Your new <?= $route ?>">
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <a href="/<?= ($route == 'Category')? 'category' : 'tag' ?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Add</button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
</div>