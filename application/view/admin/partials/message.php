<?php use Mini\Core\Session;

if (isset($_SESSION['message'])) {
    $message = Session::get('message'); ?>

    <div class="callout callout-<?= $message['type'] ?>">
        <h4><?= $message['title'] ?></h4>
        <?= $message['content'] ?>
    </div>

    <?php
    Session::delete('message');
} ?>