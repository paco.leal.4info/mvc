<?= $this->layout('layouts/adminlayout'); ?>
<div class="col-xs-5" style="margin-top: 1em;">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form action="/admin/saveGrade" class="form-horizontal" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Grade Name</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="name" name="name" placeholder="Grade Name" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Enroll Password</label>

                    <div class="col-sm-10">
                        <input class="form-control" name="enrollment" id="password" placeholder="Password" type="password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="parent" class="col-sm-2 control-label">Parent Grade</label>

                    <div class="col-sm-10">
                    <select name="parent_id" id="parent" class="form-control"><!-- Listar los que no tengan enrollment en el select -->
                        <option value="null">none</option>
                        <?php foreach ($tmpgrades as $tmpgrade):
                            if ($tmpgrade->enrollment == null): ?>
                        <option value="<?= $tmpgrade->id ?>"><?= $tmpgrade->name ?></option>
                        <?php endif;
                        endforeach; ?>
                    </select>
                    </div>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <a href="/grade" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-success pull-right">Send</button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
</div>
