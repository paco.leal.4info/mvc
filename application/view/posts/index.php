<?= $this->layout('layouts/adminlayout');
use Mini\Core\Auth;?>


<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= $title ?></h3>
                    <td><a href="/post/new" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> New</a></td>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if (isset($posts) && !empty($posts)): ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="width: 8%;">Updated</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th style="width: 8%;" class="no-sort">Preview</th>
                                <th style="width: 8%;" class="no-sort">Edit</th>
                                <th style="width: 8%;" class="no-sort">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($posts as $post): ?>
                                <tr>
                                    <td><?= $post->last_update ?></td>
                                    <td><?= $post->name ?></td>
                                    <form action="/post/publish/<?= $post->id ?>" method="post">
                                        <td class="<?= ($post->status == 'published')? 'bg-success' : 'bg-warning' ?>">
                                            <select name="status" id="status" onChange="this.form.submit()" style="width: 100%; border: 0; background: transparent;">
                                                <option value="draft" <?= ($post->status == 'published')? '' : 'selected' ?>>Draft</option>
                                                <option value="published" <?= ($post->status == 'published')? 'selected' : '' ?>>Published</option>
                                            </select>
                                            <span hidden><?= $post->status ?></span>
                                        </td>
                                    </form>
                                    <?php if ($post->status == 'published'): ?>
                                        <td><a href="/blog/article/<?= $post->slug ?>" class="btn btn-sm btn-info btn-block"><i class="fa fa-external-link"></i> Go to post</a></td>
                                    <?php else: ?>
                                        <td><a href="/blog/article/<?= $post->slug ?>" class="btn btn-sm btn-default btn-block"><i class="fa fa-eye"></i> Preview</a></td>
                                    <?php endif; ?>
                                    <td>
                                        <?php if (Auth::checkAuth('admin', false) || $post->user_id == $_SESSION['user']['id']):?>
                                            <a href="/post/edit/<?= $post->slug ?>" class="btn btn-sm btn-warning btn-block"><i class="fa fa-pencil"></i> Edit</span></a>
                                        <?php else: ?>
                                            <p class="text-center" style="color:grey; font-weight: bold; font-size: 1.5em; margin:0; padding:0;"><i class="fa fa-ban"></i></span></p>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if (Auth::checkAuth('admin', false) || $post->user_id == $_SESSION['user']['id']):?>
                                        <a href="/post/delete/<?= $post->id ?>" class="btn btn-sm btn-danger btn-block"  onClick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</span></a>
                                        <?php else: ?>
                                        <p class="text-center" style="color:grey; font-weight: bold; font-size: 1.5em; margin:0; padding:0;"><i class="fa fa-ban"></i></span></p>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <h2 class="text-secondary">There are no Posts yet</h2>
                    <?php endif; ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
