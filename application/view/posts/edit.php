<?= $this->layout('layouts/adminlayout');
use Mini\Core\Functions; ?>
<form action='/post/update<?= (isset($post))? "/$post->id" : '' ?>' method="POST" enctype="multipart/form-data">
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title"><?= $title ?></h3>
        </div>
        <div class="box-body">
    <div class="col-xs-8">
        <!-- Titulo -->
        <div class="form-group">
            <label>Title</label>
            <input class="form-control" name="name" placeholder="Enter title" type="text" value="<?= (isset($post))? "$post->name" : '' ?>">
        </div>
        <!-- CKEditor -->
        <div class="form-group">
            <label>Body</label>
            <div class="box-body pad">
                <textarea id="body" name="body" rows="10" cols="80" placeholder="Write your article"><?= (isset($post))? "$post->body" : '' ?></textarea>
            </div>
        </div>
    </div>
    <div class="col-xs-4">
        <!-- Excerpt -->
        <div class="form-group">
            <label>Excerpt</label>
            <textarea class="form-control" rows="3" name="excerpt" placeholder="Enter excerpt" style="resize:none;"><?= (isset($post))? "$post->excerpt" : '' ?></textarea>
        </div>
        <!-- Category -->
        <div class="form-group">
            <label>Category</label>
            <select class="form-control select2 select2-hidden-accessible" name="category_id" placeholder="Select your Category" style="width: 100%;" tabindex="-1" aria-hidden="true">
                <?php foreach ($categories as $category): ?>
                    <option <?= (isset($post) && ($post->category_id == $category->id))? 'selected' : '' ?> value="<?= $category->id ?>"><?= $category->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <!-- Tags -->
        <div class="form-group">
            <label>Tags</label>
            <select class="form-control select2 select2-hidden-accessible" multiple="" name="tags[]" data-placeholder="Select some Tags" style="width: 100%;" tabindex="-1" aria-hidden="true">
                <?php if (isset($post)) {
                    $post->tags = explode(',', $post->tags);
                }
                foreach ($tags as $tag): ?>
                    <option <?= ((isset($post) && (in_array($tag->id, $post->tags))) || (isset($gradeName) && $gradeName == Functions::slug($tag->name)))? 'selected' : '' ?> value="<?= $tag->id ?>"><?= $tag->name ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <?php if(isset($post) && isset($post->file)): ?>
        <div class="form-group">
            <label for="current-image">Current image</label>
            <img src="<?= $post->file ?>" class="img-responsive" id="current-image">
        </div>
        <?php endif; ?>
        <div class="form-group">
            <label for="InputFile">Select image</label>
            <input id="InputFile" type="file" name="file">
        </div>
        <div class="">
            <div class="form-group pull-left">
                <input name="status" type="checkbox" <?= (isset($post) && $post->status == 'published')? 'checked disabled' : '' ?>>
                <label for="status" style="margin: 0 0.5em;"><?= (isset($post) && $post->status == 'published')? 'Published' : 'Publish now' ?>
            </div>
            <div class="pull-right">
                <input type="submit" class="btn btn-success" value="Save">
            </div>
        </div>
    </div>
        </div>
</form>