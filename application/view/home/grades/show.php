<?= $this->layout('layouts/publiclayout');
use Mini\Core\Auth;
use Mini\Core\Functions; ?>

<section class="pages container">
    <div class="page page-about">
        <h1 class="text-capitalize"><?= $title ?>
            <?php if (!Auth::checkAuth('admin', false)): ?>
                <a href="/home/unroll/<?= $tmpgrade->id ?>" class="btn-danger pull-right" style="font-size: 50%;" onClick="return confirm('Are you sure?')">Unroll Me</a>
            <?php endif; ?>
        </h1>
        <cite>You can see any post related with <?= $tmpgrade->name ?> down below </cite>
        <div class="divider-2" style="margin: 35px 0;"></div>
        <?php if (Auth::checkAuth('teacher', false)):
            if ((isset($_SESSION['user']['grades'][0]) && $_SESSION['user']['grades'][0] == $tmpgrade->id) || Auth::checkAuth('admin', false)): ?>
                <a href="/post/new/<?= Functions::slug($tmpgrade->name) ?>" class="btn-default pull-right">New Post</a>
            <?php endif; ?>
        <?php endif; ?>
            <?php if (!isset($posts) || empty($posts)) { ?>
                <cite><?= $tmpgrade->name ?> doesn't contain posts yet</cite>
            <?php } else { ?>
                <p>Contents related to this course:</p>
                <table id="example1" class="dataTable" style="width: 100%;">
                    <thead>
                    <tr>
                        <th style="width: 10%;">Date</th>
                        <th>Title</th>
                        <th>Excerpt</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($posts as $post): ?>
                        <tr>
                            <td><?= $post->fecha ?></td>
                            <td><a href="/blog/article/<?= $post->slug ?>"><?= $post->name ?> <i class="fa fa-external-link"></i></a><span hidden><?= $post->name ?></span></td>
                            <td><?= $post->excerpt ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php //d($posts);
            }
            ?>
    </div>
</section>