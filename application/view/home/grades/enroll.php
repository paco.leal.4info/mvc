<?= $this->layout('layouts/publiclayout'); ?>

<section class="pages container">
    <div class="page page-contact">
        <h1 class="text-capitalize"><?= $title ?></h1>
        <div class="divider-2" style="margin:25px 0;"></div>
        <div class="form-contact">
            <form action="/home/singingrade/<?= $tmpgrade ?>" method="POST">
                <div class="input-container container-flex space-between">
                    <input type="password" name="password" placeholder="Course password" class="input-password">
                </div>
                <input type="submit" value="Enroll me" class="text-uppercase c-green">
            </form>
        </div>
    </div>
</section>