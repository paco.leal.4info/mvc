<?= $this->layout('layouts/publiclayout'); ?>
<section class="pages container">
    <div class="page page-about">
        <h1 class="text-capitalize"><?= $title ?></h1>
        <?php if (isset($roots) && !empty($roots)): ?>
            <cite>You are in <?= ucwords($roots['name']) ?></cite> <!-- migas de pan? -->
            <div class="divider-2" style="margin: 35px 0;"></div>
            <p>list of courses</p>
            <ul>
                <?php foreach ($roots as $keyroot => $root) {
                    if (!is_string($root) && isset($root['name'])) {
                        echo '<li><a href="/home/grades/' . $keyroot . '">' . $root['name'] . '</a></li>';
                    }
                }
                ?>
            </ul>
        <?php else: ?>
            <cite>There are no courses available</cite>
        <?php endif; ?>

    </div>
</section>
