<?= $this->layout('layouts/publiclayout'); ?>
<section class="pages container">
    <div class="page page-about">
        <h1 class="text-capitalize"><?= $title ?></h1>
        <cite>Its courses should be added sooner or later</cite>
        <div class="divider-2" style="margin: 35px 0;"></div>
        <p>We apologize, please come back soon</p>
    </div>
</section>