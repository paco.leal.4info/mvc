<?= $this->layout('layouts/adminlayout');
$section = explode('/', ($_SERVER['REQUEST_URI']));
$section = end($section);
$_SESSION['section'] = $section;
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= $title ?></h3>
                    <td><a href="/tag/new" class="btn btn-default pull-right"><i class="fa fa-plus-circle"></i> New</a></td>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if (isset($tags) && !empty($tags)): ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="width: 5%;">ID</th>
                                <th>Tag <i class="fa fa-pencil"></i></th>
                                <th style="width: 10%;" class="no-sort">Update</th>
                                <th style="width: 10%;" class="no-sort">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($tags as $tag): ?>
                                <tr>
                                    <td><?= $tag->id ?></td>
                                    <form action="/tag/update/<?= $tag->id ?>" method="POST">
                                        <td><input type="text" name="name" style="border:0; background-color: transparent; width:100%;" value="<?= $tag->name ?>"><span hidden><?= $tag->name ?></span></td>
                                        <td><button class="btn btn-sm btn-info btn-block"><i class="fa fa-floppy-o"></i> Save</button></td>
                                    </form>
                                    <td><a href="/tag/delete/<?= $tag->id ?>" class="btn btn-sm btn-danger btn-block" onClick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</span></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <h2 class="text-secondary">There are no Tags yet</h2>
                    <?php endif; ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>