Hay que utilizar la misma BBDD del proyecto de laravel, y es necesario dejar los seeders de role y user
(este último se puede modificar para tener solo al administrador, si así se desea)

Es necesario copiar al menos el directorio /public/img y sus contenidos a la aplicación correspondiente
(el logo de la aplicación y algunas imágenes internas dependen de este directorio)